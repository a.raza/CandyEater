﻿using UnityEngine;

public class Controller : MonoBehaviour
{
    #region Private Variable
    private const string
        X = "Horizontal",
        Y = "Vertical";

    private float xaxis, yaxis;
    private Vector3 m_postion;
    #endregion


    #region Unity Callbacks
    private void Update()
    {
        m_postion = Position();
    }
    #endregion

    #region Functions to Maintain controls
    private Vector2 Position()
    {
        xaxis = Input.GetAxis(X);
        yaxis = Input.GetAxis(Y);

        // if player is using the controls...
        if (IsControlerInUse())
        {
            // the virtual position will move according to the player controls
            Vector2 norm = new Vector2(xaxis, yaxis);

            return norm;
        }
        else
        {
            return Vector2.zero;
        }
    }

    private bool IsControlerInUse()
    {
        return (Input.GetAxis(X) != 0
            ||
            Input.GetAxis(Y) != 0
            ||
            Input.GetAxis(X) != 0
            &&
            Input.GetAxis(Y) != 0);
    }

    public Vector2 GetPosition()
    {
        return m_postion;
    }
    #endregion
}