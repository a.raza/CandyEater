﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : Contender
{
    #region Public Variable
    public static PlayerBehaviour instance = null;
    #endregion

    #region Unity Callbacks
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    protected override void Update()
    {
        base.Update();
    }
    #endregion
}
