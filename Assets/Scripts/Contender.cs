﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Contender : MonoBehaviour
{
    #region Public Variable
    [Range(10.0f, 25.0f)]
    public float m_speed;
    #endregion

    #region Private Variable
    private Controller m_controller;

    private Rigidbody2D m_rigidbody;

    private Camera m_camera;
    #endregion

    #region Unity Callbacks
    private void OnEnable()
    {
        PreparePlayer();
    }

    protected virtual void Update()
    {
        CamMovement();
        Controls();     // allow to use controller  
    }
    #endregion

    #region Functions to manage Player Controls
    private void Controls()
    {
        // if controller is in use.. // and not in the state of get hit
        if (IsusingControls())
        {
            //allow to rotate
            Locomote();

            //allow to move
            Move();
        }
    }

    public void CamMovement()
    {
        var position = new Vector3(transform.position.x, transform.position.y, -10);
        m_camera.transform.position = position;
    }

    //following will be used to check either player is using movement controls or not
    private bool IsusingControls()
    {
        return m_controller.GetPosition() != Vector2.zero;
    }

    ////** following vector wil be the virtual position which will always be one step ahead of the position of the player,
    //// making the movement of the character more smooth and realistic
    private Vector2 VirtualPosition()
    {
        return new Vector2
            (
                //getting the current x axis of the player, and adding +1 to the x axis.. returns one step a head of the current position of the player
                m_rigidbody.velocity.x + m_controller.GetPosition().x
                ,
                m_rigidbody.velocity.y + m_controller.GetPosition().y
            );
    }

    ////following method is being used for rotating the player
    private void Locomote()
    {
        //getting the distance between the virtual position(Whic is one step a head of the current position of the player) and the current position of the protagonsit
        var distance = VirtualPosition() - m_rigidbody.velocity;

        float angle = Mathf.Atan2(-distance.x, distance.y) * Mathf.Rad2Deg;

        //only forward, backward, left and right direction are required here
        var rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Speed() * m_speed);
    }

    private void Move()
    {
        // This will move the character according to the controller
        transform.position += new Vector3
            (
                (m_controller.GetPosition().x * Speed()) //updating horizontal movement
                ,
                (m_controller.GetPosition().y * Speed())
            );
    }

    private float Speed()
    {
        return m_speed * Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Enemy":

                if (IsusingControls() == true)  //if the controls were in use while the collision, player will die
                {
                    this.gameObject.SetActive(false);
                    
                    UIManager.instance.GameEndStats("Game Over");
                }
                else
                {
                    collision.transform.GetComponent<AIBehaviour>().enabled = false;
                    collision.gameObject.SetActive(false);
                }

                break;

            case "Candy":

                collision.gameObject.SetActive(false);
                UIManager.instance.AddScore();

                break;
        }
    }

    private void PreparePlayer()
    {
        m_camera = Camera.main;

        m_controller = GetComponent<Controller>() == null ? gameObject.AddComponent<Controller>() : GetComponent<Controller>();

        m_rigidbody = GetComponent<Rigidbody2D>() == null ? gameObject.AddComponent<Rigidbody2D>() : GetComponent<Rigidbody2D>();
    }
    #endregion
}
