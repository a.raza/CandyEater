﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Public Variable
    public static GameManager instance = null;

    public GameObject m_candyPrefab, m_enemyPrefab, m_borderPrefab;
    
    public int m_amountOfEnemies;
    #endregion

    #region Private Variable
    private const float PLAYER_STARTING_TIME = 5, CANDY_APPEAR_TIME = 0.1f;
    private const int BARRIER_SIZE = 50, SPACE_BETWEEN = 4, SPACE_BETWEEN_CANDIES = 5;

    private int m_amountOfCandies;
    private Vector3[] m_positions;
    private GameObject[] m_candies;
    private Grid m_screenSize;

    private PlayerBehaviour m_player;
    private UIManager m_uiManager;
    #endregion

    #region Unity Callbacks
    private void Awake()
    {
        if (instance == null)
            instance = this;

        m_screenSize = new Grid
        {
            m_height = BARRIER_SIZE,
            m_width = BARRIER_SIZE
        };

        m_player = PlayerBehaviour.instance;
        m_uiManager = UIManager.instance;
    }

    void Start()
    {
        var x = m_screenSize.m_width;

        var y = m_screenSize.m_height;

        x *= SPACE_BETWEEN;

        y *= SPACE_BETWEEN;

        //Create Borders
        CreateBorders(x, y);

        //Place Player
        ManagePlayerPosition(x, y);

        x = m_screenSize.m_width;
        y = m_screenSize.m_height;

        m_amountOfCandies = x * y;
        m_candies = new GameObject[m_amountOfCandies];

        //Generte the Grid Of Candies
        m_positions = GetPoistions();

        //Generate Candies
        StartCoroutine(GenerateCandies(CANDY_APPEAR_TIME));

        //Generate Enemies
        StartCoroutine(GenerateEnemies(CANDY_APPEAR_TIME));

        //Begin Game..
        StartCoroutine(CountDownDisable());

        //Activate Player Controls
        StartCoroutine(ActiveController());
    }
    #endregion

    #region Functions to prepare game
    //Will activate player Controls
    IEnumerator ActiveController()
    {
        yield return new WaitForSeconds(PLAYER_STARTING_TIME);
        m_player.gameObject.SetActive(true);
    }

    //Start the countDown
    IEnumerator CountDownDisable()
    {
        yield return new WaitForSeconds(PLAYER_STARTING_TIME - 1);
        m_uiManager.m_countDownPanal.SetActive(false);
    }

    //Will return all the array of Vector3 to which the candies will be placed 
    Vector3[] GetPoistions()
    {
        var matrix = (int)Mathf.Sqrt(m_amountOfCandies);

        var x = transform.position.x;
        var y = transform.position.y;

        var a_positions = new List<Vector3>();

        for (int row = 0; row < matrix - 1; row++)
        {
            for (int column = 0; column < matrix - 1; column++)
            {
                a_positions.Add(new Vector3(x, y, 2f));

                x += SPACE_BETWEEN_CANDIES;
            }

            x = transform.position.x;
            y += SPACE_BETWEEN_CANDIES;
        }

        return a_positions.ToArray();
    }

    //Will create and place the candies
    IEnumerator GenerateCandies(float wait)
    {
        for (int i = 0; i < m_positions.Length;)
        {
            yield return new WaitForSeconds(wait);

            for (int _i = 0; _i < m_positions.Length / 5; _i++)
            {
                if (i >= m_positions.Length)
                    break;

                m_candies[i] = Instantiate(m_candyPrefab, m_positions[i], Quaternion.identity);

                i++;
            }
        }
    }

    //Generate enemies
    IEnumerator GenerateEnemies(float wait)
    {
        for (int e = 0; e < m_amountOfEnemies; e++)
        {
            yield return new WaitForSeconds(wait);

            var _x = Random.Range(0, m_screenSize.m_height + e);
            var _y = Random.Range(0, m_screenSize.m_width + e);

            Instantiate(m_enemyPrefab, new Vector2(_x, _y), Quaternion.identity);
        }
    }

    // will place the player at the center of the grid 
    private void ManagePlayerPosition(int x, int y)
    {
        transform.position = new Vector3(x / -1.5f, y / 5, 0);

        if (m_player == null) m_player = PlayerBehaviour.instance;
        m_player.gameObject.SetActive(false);

        var _x = Random.Range(0, m_screenSize.m_height );
        var _y = Random.Range(0, m_screenSize.m_width );

        m_player.transform.position = new Vector3(_x, _y, 2);
        Camera.main.transform.position = new Vector2(0, (y + x) / 2);
        if (m_uiManager == null) m_uiManager = UIManager.instance;
        m_uiManager.transform.position = new Vector2(0, (y + x) / 2);
    }

    //Will create borders
    private void CreateBorders(int x, int y)
    {
        var topBorderPosition = new Vector2(0, y + x);
        var footerBorderPosition = new Vector2(0, 0);
        var rightBorderPosition = new Vector2(x, y);
        var leftBorderPosition = new Vector2(-x, y);

        var borderSize = new Vector3(x * 2, y / 10, 1);

        var top = Instantiate(m_borderPrefab, topBorderPosition, Quaternion.identity);
        top.transform.localScale = borderSize;
        top.name = "Top";

        var down = Instantiate(m_borderPrefab, footerBorderPosition, Quaternion.identity);
        down.transform.localScale = borderSize;
        down.name = "Down";

        var right = Instantiate(m_borderPrefab, rightBorderPosition, Quaternion.Euler(0, 0, 90));
        right.transform.localScale = borderSize;
        right.name = "Right";

        var left = Instantiate(m_borderPrefab, leftBorderPosition, Quaternion.Euler(0, 0, 90));
        left.transform.localScale = borderSize;
        left.name = "Left";

    }

    public bool IsCandyAvailable()
    {
        foreach(var candy in m_candies)
        {
            if (candy != null && candy.activeInHierarchy)
                return true;
        }

        return false;
    }
    #endregion
}

public class Grid
{
    public int m_width, m_height;
}