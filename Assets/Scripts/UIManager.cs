﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    #region Private Variable
    private const string
    FINAL_SCORE = "YourScore",
    SCORE = "Score",
    GAME_STATE = "GameState",
    COUNTER = "CountDown",
    GAME_PANEL = "Game",
    SCENE_NAME = "GameScene1";

    private Text m_scoreText, m_gameState, m_finalScore;
    private int m_score;
    #endregion

    #region Public Variable
    public static UIManager instance = null;
    internal GameObject m_countDownPanal, m_gameEndPanel;
    #endregion

    #region Unity Callbacks
    private void Awake()
    {
        if (instance == null)
            instance = this;

        m_finalScore = GameObject.Find(FINAL_SCORE).GetComponent<Text>();

        m_scoreText = GameObject.Find(SCORE).GetComponent<Text>();

        m_gameState = GameObject.Find(GAME_STATE).GetComponent<Text>();

        m_countDownPanal = GameObject.Find(COUNTER);

        m_gameEndPanel = GameObject.Find(GAME_PANEL);

        m_gameEndPanel.SetActive(false);
    }
    #endregion

    #region Event Functions
    //will add points to the player score
    public void AddScore()
    {
        m_score++;

        if (!GameManager.instance.IsCandyAvailable())
        {
            GameEndStats("Congratulations You Won!!");
        }

        m_scoreText.text = SCORE + ": " + m_score;
    }

    //Will end game
    public void GameEndStats(string _state)
    {
        m_gameState.text = _state;

        m_finalScore.text = "Your Final Score: " + m_score;

        m_gameEndPanel.SetActive(true);
    }

    //Will reset game
    public void OnPressRetry()
    {
        SceneManager.LoadScene(SCENE_NAME);
    }
    #endregion
}