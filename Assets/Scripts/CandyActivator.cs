﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandyActivator : MonoBehaviour
{
    #region Private Variable
    private const float m_miniToPush = 0.001f, m_maxToPush = 0.01f;

    private const string
        CANDY = "Candy",
        PLAYER = "Player";
    #endregion

    #region Functions to Manage activation of in game objects
    private void OnTriggerEnter2D(Collider2D _object)
    {
        if (!IsColliding(_object, PLAYER) && IsColliding(_object, CANDY))
            ActivateCandy(_object);
        else
            return;
    }

    private void OnTriggerExit2D(Collider2D _object)
    {
        if (!IsColliding(_object, PLAYER) && IsColliding(_object, CANDY))
            DeactivateCandy(_object);
        else
            return;
    }
    #endregion

    #region Helping Functions to manage activation & deactivation of in game objects
    private bool IsColliding(Collider2D _object, string _objectName)
    {
        return _object.gameObject != null && _object.gameObject.tag == _objectName;
    }

    //The candy around start moving
    private void ActivateCandy(Collider2D _object)
    {
        Rigidbody2D _rigidbody = _object.GetComponent<Rigidbody2D>();

        _rigidbody = _object.GetComponent<Rigidbody>() == null ? _object.gameObject.AddComponent<Rigidbody2D>() : _object.GetComponent<Rigidbody2D>();

        _rigidbody.gravityScale = 0;

        _rigidbody.mass = 0.0001f;

        _rigidbody.WakeUp();

        float _force = Random.Range(m_miniToPush, m_maxToPush) * Time.smoothDeltaTime;

        _rigidbody.AddForce(GetRandomDirection() * _force, ForceMode2D.Impulse);
    }

    //the candy around stop moving
    private void DeactivateCandy(Collider2D _object)
    {
        var _rigidbody = _object.GetComponent<Rigidbody2D>();

        Destroy(_rigidbody);
    }

    //Will be used to move candy in a random direction
    private Vector3 GetRandomDirection()
    {
        int randomNum = Random.Range(1, 9);

        switch (randomNum)
        {
            case 1:
                return new Vector3(0, 1);
            case 2:
                return new Vector3(1, 0);
            case 3:
                return new Vector3(1, 1);
            case 4:
                return new Vector3(-1, 1);
            case 5:
                return new Vector3(0, -1);
            case 6:
                return new Vector3(-1, 0);
            case 7:
                return new Vector3(-1, -1);
            default:
                return new Vector3(1, -1);
       }
    }
    #endregion
}
