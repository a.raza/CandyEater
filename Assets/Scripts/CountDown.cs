﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDown : MonoBehaviour
{
    #region Private Variable
    private const string TIMER = "Timer";
    private int m_timer = 0;
    private Text m_timerDisplay;
    #endregion

    #region Unity Callbacks
    void Start()
    {
        m_timerDisplay = GameObject.Find(TIMER).GetComponent<Text>();

        StartCoroutine(Counter());
    }
    #endregion

    #region Function to begin the countdown
    IEnumerator Counter()
    {
        m_timer++;

        m_timerDisplay.text = m_timer <= 3 ? m_timer.ToString() : "GO!";
        yield return new WaitForSeconds(1);
        StartCoroutine(Counter());
    }
    #endregion
}