﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AIBehaviour : MonoBehaviour
{
    #region Private Variable
    private const string CANDY = "Candy";

    private int m_points = 0;

    private float m_distance = 0;
    #endregion


    #region Public Variable
    public float m_speed;
    #endregion

    #region Unity Callbacks
    public virtual void Start()
    {

    }

    public virtual void Update()
    {
        var target = GetClosestCandy();

        if (m_distance >= 0.0f)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, Speed());

            var distance = target - transform.position;

            float angle = Mathf.Atan2(-distance.x, distance.y) * Mathf.Rad2Deg;

            var rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Speed() * m_speed);

        }
        else
            m_distance = Vector3.Distance(transform.position, target);
    }
    #endregion

    #region Event Functions
    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case CANDY:

                collision.gameObject.SetActive(false);

                m_points++;

                if (!GameManager.instance.IsCandyAvailable())
                    UIManager.instance.GameEndStats("Thank You For Playing!");
                break;
        }
    }
    #endregion

    #region Helping Functions
    private float Speed()
    {
        return m_speed * Time.deltaTime;
    }

    private Vector3 GetClosestCandy()
    {
        Vector3 candyPosition = new Vector3();

        float distance = Mathf.Infinity;

        Vector3 currentPos = transform.position;

        foreach (GameObject candy in GameObject.FindGameObjectsWithTag(CANDY))
        {
            var position = candy.transform.position;
            
            float dist = Vector3.Distance(position, currentPos);
            if (dist < distance)
            {
                candyPosition = position;
                distance = dist;
            }
        }

        return candyPosition;
    }
    #endregion
}
